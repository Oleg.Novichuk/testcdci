public with sharing class ContactListCtrl {
    @AuraEnabled(cacheable=true)
    public static List<Contact> getContactList() {
        return [
            SELECT Id, FirstName, LastName, Email
            FROM Contact
        ];
    }
}