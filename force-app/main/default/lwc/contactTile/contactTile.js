import { LightningElement, track, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners } from 'c/pubsub';

export default class ContactTile extends LightningElement {
  @track recordId;
  @wire(CurrentPageReference) pageRef;

  connectedCallback() {
    registerListener('openDetail', this.handleEvent, this);
  }

  disconnectedCallback() {
    unregisterAllListeners(this);
  }

  handleEvent(recordId) {
    this.recordId = recordId;
  }
}