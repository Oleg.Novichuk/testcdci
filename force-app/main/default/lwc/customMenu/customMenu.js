import { api, LightningElement } from "lwc";

export default class ButtonMenuOnselect extends LightningElement {
  @api rowId;
  @api selectedItemValue;

  handleOnselect(event) {
    if (event.detail.value && event.detail.value == 'showDetails') {
      this.fireShowDetail();
    }
  }

  fireShowDetail() {
    const event = CustomEvent('showdetail', {
      composed: true,
      bubbles: true,
      cancelable: true,
      detail: {
        rowId: this.rowId,
        selectedItemValue: this.selectedItemValue,
      },
    });
    this.dispatchEvent(event);
  }
}